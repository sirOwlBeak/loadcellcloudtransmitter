/* Load cell transmitter
 * 
 * auteur : Jens Sierens
 *
 * Doel van deze kleine applicatie is om een loadcell meting online beschikbaar te stellen. De loadcell zal
 * het gewicht opmeten van voedsel voor half gedomesticeerde zwerfkatten. Hierdoor moet het mogelijk worden
 * om verbruik op te meten en verdeling van nieuw voedsel te automatiseren.
 *
 * De gewichtsfunctie wacht op een ijkingscurve en transfertfunctie van een geschikte loadcell. 
 * Voorlopig geeft deze een spanning terug die op de adc wordt aangeboden door een potmeter. 
 * Enkel lokaal tareren is mogelijk op dit ogenblik.
 * Als cloud infrastructuur maak ik voorlopig gebruik van de particle cloud. In deze file is reeds een
 * kleine structurele opzet aanwezig.
 *
 * target: Particle Photon
 * compiler: gecompileerd in de cloud met Particle Build IDE
 *
 * Particle Photon dev board omvat de STM32F205RGY6 arm cortex M3 processor met 120 MHz clock,
 * alsook een Broadcom BCM43362 wifi chip. Firmware kan zowel offline als online gecompileerd worden.
*/

#include "math.h"
#include "clickButton/clickButton.h"

#define SENSE_PIN           A0
#define THERMISTOR_PIN      A1
#define MANUAL_TARE_PIN     D0

/* adc.h */
const int ADC_RESOLUTION = 800; // µV
int readSensePin(int, int (*filter)(int []));
/**/

/* filters.h */
const unsigned int AVERAGE_DIVIDER = 3;
const unsigned int AVERAGE_COUNT = 1 << AVERAGE_DIVIDER;
int average(int []);
/**/

/* loadcell.h */
//const float LOADCELL_CAPACITY = 6000.0; // gram
//const float LOADCELL_OUTPUT = 1000.0; // µV/V
double getMetricWeight(int, int);
void tare(int*);
void zeroTara(int*);
/**/

/* analogtemp.h */
const unsigned int THERMISTOR_B_COEFFICIENT = 3950; // B at To = 25 degrees C
const unsigned int THERMISTOR_R_NOMINAL = 10000; // R at To = 25 degrees C
const unsigned int THERMISTOR_T_NOMINAL = 25; // degrees celsius
const unsigned int THERMISTOR_R_SERIAL = 10000; // resistor in series with thermistor, in ohms
double getTemperature(int);
/**/

/* application constants */
// timer periods in milliseconds
const unsigned int LOADCELL_SAMPLE_PERIOD = 20;
const unsigned int BUTTON_POLLING_PERIOD = 20; 

/* application globals */
double temperature;
double metricWeight;
int tara;

ClickButton ManualTareButton(MANUAL_TARE_PIN, LOW, CLICKBTN_PULLUP);
Timer sampleTimer(LOADCELL_SAMPLE_PERIOD, SampleWeight);

void setup()
{
    pinMode(SENSE_PIN, INPUT);
    pinMode(MANUAL_TARE_PIN, INPUT_PULLUP);
    
    // todo: tara is geen continu veranderlijke, misschien beter om deze als event te publishen.
    tara = 0;
    Particle.variable("Tara", tara);
    
    metricWeight = 0.0;
    Particle.variable("MetricWeight", metricWeight);
    
    temperature = 0.0;
    Particle.variable("Temperature", temperature);
    
    // start the software timers
    sampleTimer.start();
}

void loop()
{
    ManualTareButton.Update();
    int clicks = ManualTareButton.clicks;
    if (clicks > 0)
    {
        tare(SENSE_PIN, &tara);
    }
    else if (clicks < 0)
    {
        zeroTara(&tara);
    }
    delay(100);
}

// sampleTimer callback
// modifies application globals
void SampleWeight()
{
    metricWeight = getMetricWeight(SENSE_PIN, tara);
    temperature = getTemperature(THERMISTOR_PIN);
}

/* analogtemp.c */

// reads the analogpin voltage and uses a steinhart conversion formula for returning temperature value
// good basis but seems to have an error of a few degrees around room temperature
double getTemperature(int analogPin)
{
    int adcval = readSensePin(analogPin, average);
    double resistance;
    resistance = ( (float)4095 / adcval ) - 1;
    resistance = (float)10000 / resistance;
    double steinhart;
    steinhart = resistance / THERMISTOR_R_NOMINAL;
    steinhart = log(steinhart);
    steinhart /= THERMISTOR_B_COEFFICIENT;
    steinhart += 1.0 / ( THERMISTOR_T_NOMINAL + 273.15 );
    steinhart = 1.0 / steinhart;
    steinhart -= 273.15;
    return steinhart;
}
/**/

/* loadcell.c */

// Reads an analog pin and returns the voltage in µV
// tara: value after latest tare call.
// TODO: metrisch massa conversie implementeren wanneer gepaste loadcell beschikbaar is.
double getMetricWeight(int analogPin, int tara)
{
    int sense = readSensePin(analogPin, average);
    double voltage = ( sense - tara ) * ADC_RESOLUTION;
    return voltage;
}

// Samples the sense pin and stores the value as a tara value in a referenced variable.
void tare(int analogPin, int* taraVariable)
{
    *taraVariable = readSensePin(analogPin, average);
}

// Function that zeroes the referenced tara variable.
void zeroTara(int* taraVariable)
{
    *taraVariable = 0;
}
/**/

/* filters.c */

// Returns the arithmetic average of an array of int
// samples[] = assumed to contain at least AVERAGE_COUNT items.
int average(int samples[])
{
    // 32 bit variable to prevent overflow during summation.
    unsigned int accumulator = 0;
    for (short i = 0; i < (short)AVERAGE_COUNT; i++)
    {
        accumulator += samples[i];
    }
    return (int)(accumulator >> AVERAGE_DIVIDER);
}
/**/

/* adc.c */

// Samples the analogPin a number of times and returns the filtered measurements.
// analogPin: pin number for adc sampling
// (*filter): filtering function that takes a single array of ints and returns an integer
int readSensePin(int analogPin, int (*filter)(int []))
{
    int samples[AVERAGE_COUNT] = {0};
    for (short i = 0; i < (short)AVERAGE_COUNT; i++)
    {
        samples[i] = analogRead(analogPin);
    }
    return filter(samples);
}
/**/