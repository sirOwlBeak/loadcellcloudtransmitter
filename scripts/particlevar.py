#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 19:17:04 2016

@author: Jens Sierens
"""


def main(argv, *args):
    
    import urllib
    
    requestFunction = args[1]
#    metrics = {
#        'Temperature' : 'degrees C',
#        'MetricWeight' : 'volt',
#    }
    
    try:    
        response = requestFunction(argv.variable)
        #metric = jsondecoder().decode(response)
    except urllib.error.HTTPError as e:
        sys.stdout.error(e)
    
    sys.stdout.write(response)
    

if __name__ == "__main__":
    
    import os, sys
    sys.path.append(os.getcwd())
    
    from ParticleCloudUtils import ParticleAuthenticator, ParticleCloud
    import argparse
    
    parser = argparse.ArgumentParser(
        description = 'Request a Particle Variable from the cloud.'
        )
    
    parser.add_argument('variable', 
        action="store"
        )
    
    parser.add_argument('-d', '--device-id', 
        action="store",
        dest="deviceId"
        )
    
    parser.add_argument('-a','--access-token', 
        action="store", 
        dest="accessToken"
        )
    argv = parser.parse_args()
    
    authenticator = ParticleAuthenticator(argv.deviceId, argv.accessToken)
    requestFunction = ParticleCloud(authenticator.deviceId, authenticator.accessToken)
    main(argv, authenticator, requestFunction)
    
    sys.exit()