#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 11 15:37:19 2016

@author: Sierens Jens

Deze file bevat functionaliteiten voor het maken van requests aan de cloud api.

De file is uitvoerbaar als script:
    script vraagt een particle variable op in de cloud
    command line argument:
    -m, --metric : geef naam van Particle Variable op ie 'Temperature' of
                    'MetricWeight'
"""
from urllib.parse import urlencode
from urllib.request import Request, urlopen
from urllib.error import URLError
from string import Template


class ParticleAuthenticator:
    def __init__(self, deviceId, accessToken):
        self.deviceId = deviceId
        self.accessToken = accessToken


def ParticleCloud(deviceId, accessToken, *args):
    ''' Returns a function object to request variables from the particle cloud
    arguments:    
    deviceId = String that identifies a Particle board in the cloud
    accessToken = String serving as token for connecting to the cloud
    '''
    root = 'https://api.particle.io'
    get = '/v1/devices/$device/$variable?$args'
    # the particle GET url is partially applied with safe_substitute
    url = Template(root + get)    
    url = Template(url.safe_substitute(
        device = deviceId,
        args = urlencode({'access_token':accessToken})))
    
    def GetRequest(variableName):
        ''' Returns a single Particle variable as json compatible string.
        arguments:
        variableName
        '''
        fullurl = url.substitute(variable=variableName)
        request = Request(fullurl)
        try:
            response = urlopen(request)
            return response.read().decode()
        except URLError as e:
            if hasattr(e, 'reason'):
                print("Could not reach the server!")
                print("Reason: ", e.reason)
            elif hasattr(e, 'code'):
                print("Server could not fulfill the request!")
                print("Error code: ", e.code)
            raise
    
    return GetRequest


if __name__ == '__main__':
    pass